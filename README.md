# Polycrystal Model Generator for Abaqus/CAE

A Python-based tool for generating polycrystal models within Abaqus/CAE, designed specifically for Crystal Plasticity Finite Element Method (CPFEM) simulations. This tool enables the creation of polycrystalline structures for complex geometries, with control over grain morphology and crystallographic texture.

For a comprehensive description of the underlying algorithms and implementation details, please refer to our published research paper cited below.

## Features

- Generate polycrystal models for arbitrary complex geometries in Abaqus/CAE
- Control grain size, shape, and orientation
- Define crystallographic texture
- Support for anisotropic grain growth
- Compatible with CPFEM analysis
- Designed for both simple and complex geometries, including lattice structures
- Integrates with Abaqus/Standard or Abaqus/Explicit through UMAT or VUMAT implementations

## Usage

The tool works with meshed geometries in Abaqus/CAE and follows these main steps:

1. Extract coordinates of mesh nodes and element-node connectivity
2. Generate random seeds inside in the model
3. Assign elements to grains based on growth algorithms
4. Define crystallographic orientations for each grain
5. Create element sets in Abaqus for CPFEM analysis


To use this tool, begin with your meshed geometry in Abaqus/CAE and Modify the configuration parameters in the script to specify the model and instant names and desired grain size. Execute the script through Abaqus/CAE's 'Run Script' feature.
## Requirements

- Abaqus/CAE
- Python (compatible with Abaqus/CAE Python interpreter)
- Access to Abaqus user subroutine capabilities (for UMAT implementation)


## Example Applications

The tool has been successfully tested with:
- Lattice structures with different unit cells
- Complex geometries (e.g., mini-impeller sections)
- Various crystallographic textures
- Different grain morphologies

![img](https://i.imgur.com/4cbKZSg.png)

## Contributors

Tomas Manik (Researcher), Hassan Moradi (PhD Candidate), Bjørn Holmedal (Professor) at
Physical Metallurgy Group at the Department of Materials Science and Engineering, 
Odd Sture Hopperstad, Professor at Department of Structural Engineering

Norwegian University of Technology, Trondheim, Norway.

This project is part of the METPLAST project and received support from the Research Council of Norway. Contributions are welcome through merge requests.

## Citation

If you use this tool in your research, please cite:

[![DOI](DOI)](DOI)

```
PAPER UNDER REVIEW
```

## Contact

Tomas Manik - tomas.manik@ntnu.no

Hassan Moradi - hassan.m.asadkandi@ntnu.no

