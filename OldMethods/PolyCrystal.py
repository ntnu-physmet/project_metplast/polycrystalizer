import sys
import os
import numpy as np
import pandas as pd
import utils_inp as ui
cwd = os.getcwd()
# ================================= INPUTS ====================================
# =============================================================================
# --------- Working Directory
cwd = "C:\\UMAT\\CPFEM_YS\\Inp_files\\RVE10_lars\\Model\\"
cwd = cwd.replace('\\', '/')
# ========================= Refence Block Definition ==========================
# ASSUMING DIMENSIONS IN REF BLOCK ARE IN mm
#  - Output from Dream3D, to construct the reference block,
#    including the following files:
#       - InputFile_nodes.inp
#       - InputFile_elems.inp
#       - InputFile_elset.inp
file_nodes = cwd+'/' + 'CPFEM_size10_nodes.inp'
file_elems = cwd+'/' + 'CPFEM_size10_elems.inp'
file_elset = cwd+'/' + 'CPFEM_size10_elset.inp'
#   - The Resolution (Cell Spacing) used in Dream3D in mm, for each axis:
Res = np.array([100/1000, 100/1000, 100/1000])
# =============================================================================
# ============================= Model Definition ==============================
#   - The abaqus input file with model and mesh info, to construct the model:
#   - instant name of the part in the assembly:
include_instantname= False
file_model = cwd + '/' + 'CPFEM_RVE_geom.inp'
instantname = 'UnitCell'
instantname = ', instance=' + instantname + '-1'

# Number of nodes in each element
Model_Element_nodes_n=8
#   For orientation:
#   NOTE: Two different Approaches for making input file:
#   1- Orientation angles are passed as material property. then file Output_Sects should be used,
#   and for each Elset a seperate material should be defined. No need for Output_InitialSDVs
#   2- Orientation angles are passed as Initial values of certain SDVs. then file Output_InitialSDVs should be used,
#   and whole model should be defined as one Elset, with one material assigned to that. No need for Output_Sects
Approache = 2
#   - for Approache=2
#       - Number of SDVs
#       - Srating index of Euler Angles in SDVs
#       - file containing Euler angles, should have at least as many orientations as grains.
NSDVs = 100
Phis_indx = 1   # RMP:8 , SUB:1, Frodal:1
Euler_file_name = 'Eulers_Rand_100k.txt'
# Eventually, expected Outputs:
# Output_Elset.inp                        : element set info
# if Approache=1 > Output_Sects.inp       : section info
# if Approache=2 > Output_InitialSDVs.inp : SDV initialization
# =============================================================================
# Save a reference to the original standard output
original_stdout = sys.stdout
def SelectSection(Fname, Secname, Rshape):
    flist = ui.read(Fname)
    Sec_datas = []
    # Reading Section Info
    for i in range(len(flist)):
        if flist[i][0].lower()==Secname:
            Sec_data=flist[i][1]['data']
            Sec_data=np.array(Sec_data)
            if Rshape!=0:
                Sec_data=Sec_data.reshape(int(len(Sec_data)/Rshape),Rshape)
            Sec_datas.append(Sec_data)
    return Sec_datas

def RotationMatrix(Ax,Ang):
    # Rotation Matrix for Rotation Along Ax ('X' or 'Y' or 'Z') , Ang in degree
    Ang=np.deg2rad(Ang)
    if Ax=='X': ax=0
    if Ax=='Y': ax=1
    if Ax=='Z': ax=2
    Mat=np.zeros(shape=(3,3))
    Mat[ax,ax]=1
    ij=[]
    for i in range(3):
        if i!=ax:
            ij.append(i)
            Mat[i,i]= np.cos(Ang)
    Mat[ij[0],ij[1]]= -1*np.sin(Ang)
    Mat[ij[1],ij[0]]= 1*np.sin(Ang)
    return Mat
#%% Section 1 (Block)
#
print('step 1 of 6 ----------------------------------------------------------')
print('------->> Loading Reference Block...')
# Reading Nodes file
# The Assumption is that the Node Ids are in ascening order
Block_Nodes= SelectSection(file_nodes, 'node',4)[0][:,1:]
# (!!!!!!) NOTE
# in some version of dream3D, since it would make an extra useless node at the
# end of the node list, you should go: [0][:-1,1:] to exclude it.
# Conversion from Micron to mm
# Block_Nodes=Block_Nodes/1000
N_Block_Nodes=len(Block_Nodes)
print ('Number of Nodes in the Block:',N_Block_Nodes)
# Reading Element file
# The Assumption is that the Element Ids are in ascening order
Block_Elems= SelectSection(file_elems, 'element',9)[0][:,1:]
N_Block_Elems=len(Block_Elems)
print ('Number of elements in the Block:',N_Block_Elems)
# Reading Element Set file
Block_Elsets= SelectSection(file_elset, 'elset',0)[:]
# (!!!!!!) NOTE
# in some version of dream3D, it will make an extra elset which is the whole model,
# we only want to have grains, so you should go: [1:] to exclude it.
# El_to_Elset is an array size the number of elements which holds the grain ID for the element ID=index+1
El_to_Elset=np.zeros(N_Block_Elems)
for i,BE in enumerate(Block_Elsets):
    for BE0 in BE:
        El_to_Elset[BE0-1]=i+1
N_Block_Elsets=len(Block_Elsets)
print ('Number of Grains in the Block:',N_Block_Elsets)
print ('Dimentions of the Block:')
Bounds0=np.zeros(shape=(3,2)); Cen0=np.zeros(3)
for i in range(3):
    Bounds0[i][0]=min(Block_Nodes[:,i])
    Bounds0[i][1]=max(Block_Nodes[:,i])
    Cen0[i]=np.average(Bounds0[i])
    print ('Min & Max Along Axis', i+1,':', Bounds0[i][0],Bounds0[i][1])
#
#%% Section 1 (Block)
print('step 2 of 6 ----------------------------------------------------------')
print('------->> Finding Centers of Block Elements...')
Block_Elems=Block_Elems.astype(int)
Elem_Cents=np.zeros(shape=(N_Block_Elems,3))

for i in range(3):
    Elem_Cents[:,i]=np.average(Block_Nodes[Block_Elems-1,i],axis=1)
#
#%% Section 1 (Block)
print('step 3 of 6 ----------------------------------------------------------')
print('------->> Preparing Block, Rendering Element IDs as a 3D array...')
Block_Dim=np.zeros(3)
for i in range(3):
    Block_Dim[i]=np.round(max(Block_Nodes[:,i])/Res[i],6)
Block_Dim=Block_Dim.astype(int)
Elem_IDS=np.arange(N_Block_Elems)+1
Block_Net=Elem_IDS.reshape(Block_Dim[0],Block_Dim[1],Block_Dim[2], order='F').astype(int)
#
#%% Section 2 (MODEL)
#==============================================================================
print('step 4 of 6 ----------------------------------------------------------')
print('------->> Reading Model info...')
Model_Nodes= SelectSection(file_model, 'node',4)[0][:,1:]
N_Model_Nodes=len(Model_Nodes)
print ('Number of Nodes in the Model:',N_Model_Nodes)
# -----------
# Reading Element file
Model_Elems= SelectSection(file_model, 'element',Model_Element_nodes_n+1)[0][:,1:]
N_Model_Elems=len(Model_Elems)
print ('Number of elements in the Model:',N_Model_Elems)
# -----------
# If Model Requires Rotation
#RotVec=RotationMatrix('X',90)
#for i,xyz in enumerate(Model_Nodes):
#    Model_Nodes[i]=np.matmul(RotVec, xyz)

# Reading Element Set file
print ('Dimentions of the Model:')
Bounds=np.zeros(shape=(3,2)) ; Cen=np.zeros(3)
for i in range(3):
    Bounds[i][0]=min(Model_Nodes[:,i])
    Bounds[i][1]=max(Model_Nodes[:,i])
    Cen[i]=np.average(Bounds[i])
    print ('Min & Max Along Axis', i+1,':', Bounds[i][0],Bounds[i][1])
print ('Shifting Models center to that of the Block :')
print ('New Dimentions of the Model:')
# Also Checking if the Block Encloses the entire Model
Enclosed=True
for i in range(3):
    Model_Nodes[:,i] += Cen0[i]-Cen[i]
    Bounds[i][0]=min(Model_Nodes[:,i])
    Bounds[i][1]=max(Model_Nodes[:,i])
    print ('Min & Max Along Axis', i+1,':', Bounds[i][0],Bounds[i][1])
    if (Bounds[i][0]<Bounds0[i][0]) or (Bounds[i][1]>Bounds0[i][1]):
        Enclosed=False
if Enclosed:
    print('Model is entirely enclosed by the reference block now.')
else:
    print('!!!! Error: Model is larger than the Block !!!!')
    sys.exit()

print('------->> Finding Centers of Model Elements...')
MElem_Cents=np.zeros(shape=(N_Model_Elems,3))
for t in range(N_Model_Elems):
    for i in range(3):
        MElem_Cents[t,i]=np.average(Model_Nodes[Model_Elems[t].astype(int)-1,i])
#%% Section 3 (Solution)
#
print('step 5 of 6 ----------------------------------------------------------')
print('------->> Finding the Nearest Block elemet for each Model element ...')
# el_Conectivity[:,0] will be the element id in Block nearest to the model element with id=index+1
# el_Conectivity[:,1] will be Grain ID where block element falls in
el_Conectivity=np.zeros(shape=(len(Model_Elems),3))
Loc=np.zeros(3)
Err=np.zeros(3) ; MaxErr=np.zeros(3)
for t in range(len(Model_Elems)) :
    for i in range(3):
        Loc[i] = np.round((MElem_Cents[t,i]-Elem_Cents[0,i])/ Res[i],0)
    Loc=Loc.astype(int)
    el_Conectivity[t,0] = Block_Net[Loc[0],Loc[1],Loc[2]]
    Err=np.abs(MElem_Cents[t]-Elem_Cents[int(el_Conectivity[t,0])-1])
    MaxErr=np.maximum(Err,MaxErr)
print('------->> Maximum distance between centers:', round(max(MaxErr*1000),2),'microns')
el_Conectivity=el_Conectivity.astype(int)
print('------->> Finding the corresponding Grain ID in the block ...')
print(max(el_Conectivity[:,0]))
el_Conectivity[:,1]= El_to_Elset[el_Conectivity[:,0]-1]
el_grains=np.unique(el_Conectivity[:,1])
print('------->> Of the total', N_Block_Elsets,'Grains in the block,',len(el_grains),'Grains happen to be in the Model.')
#el_grains = (np.arange(len(eu))+1).astype(str)
#el_grains = eu.astype(str)
#def GrainText(i):
#    return
#vGrainText=np.vectorize(GrainText)
#el_grains=vGrainText(el_grains)
#%% Section 4 (Outputs)
#
print('step 6 of 6 ----------------------------------------------------------')
# ----------------------------------------------------------
print('------->> Making input file: Output_Elset.inp ...')
with open(cwd+'/' + 'Output_Elset.inp', 'w') as outfile:
    sys.stdout = outfile # Change the standard output to the text file
    print('** Element sets')
    print('**------------------------------------------------------------------------------')
    totalindxs=[]
    for i in range(len(el_grains)):
        Gr=i+1
        if include_instantname:
            print('*Elset, elset=Grain-Set-'+str(Gr) +instantname)
        else:
            print('*Elset, elset=Grain-Set-'+str(Gr))
        indxs=np.where(el_Conectivity[:,1]==el_grains[i])[0]+1
        txts=[] ; txt='' ; cntr=0
        for indx in indxs:
            if txt!='': txt +=','
            txt +=str(indx)
            cntr +=1
            if cntr==16:
                txts.append(txt)
                txt='' ; cntr=0
        if txt!='': txts.append(txt)
        for txti in txts:
            print(txti)
    print('**------------------------------------------------------------------------------')
sys.stdout = original_stdout # Reset the standard output to its original value
# ----------------------------------------------------------
if Approache==1:
    print('------->> Making input file: Output_Sects.inp ...')
    with open(cwd+'/' + 'Output_Sects.inp', 'w') as outfile:
        sys.stdout = outfile # Change the standard output to the text file
        print('** Each section is a separate grain')
        print('**------------------------------------------------------------------------------')
        for i in range(len(el_grains)):
            Gr=i+1
            print('** Section: Sec Grain '+str(Gr))
            print('*Solid Section, elset=Grain-Set-'+str(Gr),', controls=EC-1, material=Grain_Mat'+str(Gr))
            print(',')
        print('**------------------------------------------------------------------------------')
    sys.stdout = original_stdout # Reset the standard output to its original value
# ----------------------------------------------------------
if Approache==2:
    print('------->> Making input file: Output_InitialSDVs.inp ...')
    print('------->> Reading Euler Orientations File...')
    Eulers=pd.read_csv(cwd+'/'+Euler_file_name, header=None, delim_whitespace=True)
    Eulers=np.array(Eulers)[:len(el_grains)]
    with open(cwd+'/' + 'Output_InitialSDVs.inp', 'w') as outfile:
        sys.stdout = outfile # Change the standard output to the text file
        print('** Defining Initial Values of Euler Angles')
        print('**------------------------------------------------------------------------------')
        print('*Initial conditions, type=SOLUTION')
        print('** elset, phi1, PHI, phi2')
        for i in range(len(el_grains)):
            Gr=i+1
            elset='Grain-Set-'+str(Gr)
            j=1 ; txt=''
            NL=8 if NSDVs>8-1 else NSDVs+1
            while j<NSDVs+1:
                if j==1:
                    txt=elset
                    NLx=1
                else:
                    NLx=0
                for k in range(NL-NLx):
                    j=j+1
                    valtxt=str(Eulers[i,j-Phis_indx-1]) if ((j>=Phis_indx+1) and (j<=Phis_indx+2+1)) else '0.0'
                    if j<=NSDVs+1:
                        txt += ', ' if txt!='' else txt
                        txt += valtxt
                print(txt)
                txt=''
    sys.stdout = original_stdout # Reset the standard output to its original value
print ('------->> Done!')