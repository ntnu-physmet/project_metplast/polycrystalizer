# Polycrystalizer
*For Crystal Plasticity simulations of complex geometries*
![img](https://i.imgur.com/Wkh5lkh.png)


*Poster presentation of an application of this tool in LightMat2023 conference:*
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.8278627.svg)](https://doi.org/10.5281/zenodo.8278627)

- ToDo:
  - [x] cleaning up the code
  - [x] improving the interface
  - [x] example files
  - [x] ...