import os
import sys
import numpy as np
import utils_inp as ui
import matplotlib.pyplot as plt
cwd = os.getcwd()
original_stdout = sys.stdout # Save a reference to the original standard output
#==============================================================================
#----------------------------------- INPUTS -----------------------------------
# Directory where Model Node file is
# Model should be a square hollow tube, and can have edges rounded
cwd=r'C:/UMAT/CrashTube2/InputFiles_NT/'
file_model= cwd+'/' + 'Tube_Nodes.inp'
# Dimensions of the Tube
H=9
a_out=3
a_in=2.5
# b=a-2*r where r is the edge fillet radius
b=2
# Bulging factor m=bulging/a_in, whete t is the wall thickness: 0.01-0.2
m=0.05
# Number of the bulges along the Height
N=2
#
#==============================================================================
#--------------------------------- FUNCTIONS ----------------------------------
def SelectSection(Fname, Secname,Rshape):
    flist=ui.read(Fname)
    Sec_datas=[]
    # Reading Section Info
    for i in range(len(flist)):
        if flist[i][0].lower()==Secname:
            Sec_data=flist[i][1]['data']
            Sec_data=np.array(Sec_data)
            if Rshape!=0:
                Sec_data=Sec_data.reshape(int(len(Sec_data)/Rshape),Rshape)
            Sec_datas.append(Sec_data)
    return Sec_datas

def normal_distribution(x, mean, std_deviation):    
    y = (1 / (std_deviation * np.sqrt(2 * np.pi))) * np.exp(-0.5 * ((x - mean) / std_deviation) ** 2)
    return y
  
def Read_The_Mode_Nodes(file_model):
    print('------->> Reading Model info...')
    Model_Nodes= SelectSection(file_model, 'node',4)[0][:,0:]
    N_Model_Nodes=len(Model_Nodes)
    print ('Number of Nodes in the Model:',N_Model_Nodes)    
    # -----------
    print ('Dimentions of the Model:')      
    Bounds=np.zeros(shape=(3,2)) ; Cen=np.zeros(3)
    for i in range(3):
        Bounds[i][0]=min(Model_Nodes[:,i+1])
        Bounds[i][1]=max(Model_Nodes[:,i+1])
        Cen[i]=np.average(Bounds[i])     
        print ('Min & Max Along Axis', i+1,':', Bounds[i][0],Bounds[i][1])
    print(np.shape(Model_Nodes))    
    return Model_Nodes,N_Model_Nodes

def GetBulge(x,b,m):
    # making sure the curve will be flat at x=b/2 and x=-b/2
    ff=0.01 # y at x=b/2 or -b/2 will be ff times ymax
    std_deviation=b/(2*np.sqrt(-2*np.log(ff)))
    mean= 0
    Bmax=1 / (std_deviation * np.sqrt(2 * np.pi))
    Bfactor=(m*a_in)/Bmax
    #
    to_return=Bfactor*normal_distribution(x, mean, std_deviation)
    if hasattr(to_return, "__len__"):
        # a numpy Array
        indx=np.where(abs(x)>b/2)[0]        
        to_return[indx]=0
    else:
        # Scalar
        if abs(x)>b/2: to_return=0     
    return to_return

def Vertical_Bulge(xh):    
    x=xh%(H/N)
    x=x-(H*0.5/N)
    y=GetBulge(x,0.95*(H/N),m)
    no=(xh/(H/N)).astype(int)
    osl=2*(no%2)-1
    return osl*y    

def Plot_Result():
    # Height
    fig, axH = plt.subplots(figsize=(18, 4))
    xh = np.linspace(0, H, 500)
    yh=Vertical_Bulge(xh)      
    axH.plot(xh,yh,color='b')
    axH.set_aspect('equal')
    axH.set_xlim([0, H]);axH.set_ylim([-a_out/3, a_out/3])    
    plt.show()
    
    # Cross Section
    fig, axS = plt.subplots(figsize=(8, 8))
    x = np.linspace(-b/2, b/2, 100)
    max_m_all=GetBulge(0,b,m)
    for iH in np.linspace(0,H,(N*2*8+1)):
        max_m=Vertical_Bulge(iH)
        mi=m*max_m/max_m_all
        y=GetBulge(x,b,mi)    
        axS.plot(x,-y+a_out/2,color='b'); axS.plot(x,y-a_out/2,color='b')
        axS.plot(y+a_out/2,x,color='b'); axS.plot(-y-a_out/2,x,color='b')
    axS.set_aspect('equal')
    axS.set_xlim([-a_out/1.5, a_out/1.5]);axS.set_ylim([-a_out/1.5, a_out/1.5])    
    plt.show()   
    
def Deform(Model_Nodes):
    # Applying the bulging
    unique_Z=np.unique(Model_Nodes[:,3])
    max_mz_all=GetBulge(0,b,m)
    for Zi in unique_Z:
        Unique_Z_indx=np.where(Model_Nodes[:,3]==Zi)
        max_mz=Vertical_Bulge(Zi)
        mzi=m*max_mz/max_mz_all
        #
        Xs,Ys=Model_Nodes[Unique_Z_indx,1],Model_Nodes[Unique_Z_indx,2]
        indx1=np.where(Xs>=abs(Ys))
        indx2=np.where(Ys>abs(Xs))
        indx3=np.where(-Xs>=abs(Ys))
        indx4=np.where(-Ys>abs(Xs))
        # Region 1 & 3
        Xs[indx1]=Xs[indx1]-GetBulge(Ys[indx1],b,mzi)
        Xs[indx3]=Xs[indx3]+GetBulge(Ys[indx3],b,mzi)
        # Region 2 & 4
        Ys[indx2]=Ys[indx2]+GetBulge(Xs[indx2],b,mzi)
        Ys[indx4]=Ys[indx4]-GetBulge(Xs[indx4],b,mzi)          
        Model_Nodes[Unique_Z_indx,1],Model_Nodes[Unique_Z_indx,2]=Xs,Ys    
    return Model_Nodes
#
#==============================================================================
#-------------------------------- PROGRAM BODY---------------------------------
# Just to see how is the choice of parameter m gonna work
#Plot_Result()
#%%
# Loading the Node List of the Model
Model_Nodes,N_Model_Nodes=Read_The_Mode_Nodes(file_model)
Model_Nodes=Deform(Model_Nodes)

fig, axZ = plt.subplots(figsize=(8, 8))
axZ.plot(Model_Nodes[:,1],Model_Nodes[:,2],'.',color='blue')
plt.show() 

#
#==============================================================================
#-------------------------------- OUTPUT --------------------------------------
# Writing the new node fils
# Reading initial Lines from the model file:
Lines=  open(file_model, 'r').readlines()
Lines0=[]
for Line in Lines:
    if Line[:5]!='*Node':
        Lines0.append(Line.strip())
    else:
        Lines0.append('*Node')
        break
    
fname=file_model.split('.')[0]+'_Def'+'.inp'    
print('------->> Making input file: '+fname) 

with open(fname, 'w') as outfile:
    sys.stdout = outfile # Change the standard output to the text file
    for Line in Lines0:
        print(Line)
    for M in Model_Nodes:
        print('%d, %.10f,  %.10f,  %.10f' %(M[0],M[1],M[2],M[3]))

sys.stdout = original_stdout # Reset the standard output to its original value 
print ('------->> Done!')
#
#==============================================================================


