from abaqus import *
import numpy as np

# Inputs
# -----------------------------------------------------------------------------
# Model and instant name
model_name = 'Model-1'
instant_name = 'instant-1'
grainsize = 200.0  # in micrometer
Nsdv = 100         # number of state variables
default_amp = 1.   # default global elongation factor
# -----------------------------------------------------------------------------

def generate_seeds(coords, Ngrains):
    # 1st method:
    """
    # Creating random seeds inside the entire envoloping cuboid
    # Some seeds will be outside the geometry, therefore number of grains will
    # be less than number of seeds!
    minpoint = np.array([1.e10, 1.e10, 1.e10])
    maxpoint = np.array([-1.e10, -1.e10, -1.e10])
    for coord in coords:
        # coord = np.array(node.coordinates)
        minpoint[coord < minpoint] = coord[coord < minpoint]
        maxpoint[coord > maxpoint] = coord[coord > maxpoint]
    print("Corners of the Cuboid that will envelop the whole model:", minpoint, maxpoint)
    # Generating random seeds inside the enveloping cuboid volume
    seeds = minpoint + np.random.rand(Ngrains, 3)*(maxpoint-minpoint)
    """
    # 2nd method:
    # Choosing Ngrains nodes from all the nodes of the mode, randomly, as seeds
    # All seeds will be guaranteed to be inisde the geometry, or on the surface.
    seeds = coords[np.random.randint(coords.shape[0], size=Ngrains), :]
    return seeds

def poly(ele_centers, seeds, pathcoords, pathamps):
    def log_av(a, b):
        return 10**((np.log10(a)+np.log10(b))/2)
    # Returns an array size of number of elements, containing ids of seeds each
    # element should belong to.
    for k, seed in enumerate(seeds):
        print("Constructing Grain No %s" %(k+1))
        vec = ele_centers-seed
        distance = np.sum(vec**2, axis=1)
        av_amp, av_vel , weight_sum= 0, [0, 0, 0], 0
        # finding velocity using defined path and amplitudes
        for p, pathcoordi in enumerate(pathcoords):
            # Finding the midpoint of path nodes
            pathcoord = (pathcoordi[:-1, :] + pathcoordi[1:, :])/2
            dist_to_path = np.sum((pathcoord-seed)**2, axis=1)
            dist_min, min_index = np.min(dist_to_path), np.argmin(dist_to_path)
            weight = 1/max(dist_min, 1.e-6)
            # Direction and amplitude of the closest midpoint on current path to current seed
            av_amp += np.log10(log_av(pathamps[p][min_index], pathamps[p][min_index+1]))*weight
            closests_vec = pathcoordi[min_index+1] - pathcoordi[min_index]
            av_vel += (closests_vec / np.linalg.norm(closests_vec))*weight
            weight_sum = weight_sum + weight
        av_amp, av_vel = 10**(av_amp/weight_sum), av_vel/weight_sum
        # Velocity vector is now found for each seed
        vel = av_vel / np.linalg.norm(av_vel)
        vec  = vec / np.linalg.norm(vec, axis=1)[:,None]
        dot_product = np.dot(vec, vel)
        cos2t = dot_product**2
        r = np.sqrt(1/(cos2t*(1/av_amp**2-1)+1))
        distance = distance / r
        if k == 0:
            mindists0 = distance
            mins = np.zeros(ele_centers.shape[0])
        else:
            mindists = np.min(np.concatenate(([mindists0], [distance])), axis=0)
            mins[np.where(mindists0 != mindists)[0]] = k
            mindists0 = mindists
    return mins.astype(int)


print("=========================== S T A R T ================================")
print("---->  Reading model nodes and elements...")
myModel = mdb.models[model_name]
assembly = myModel.rootAssembly
part = assembly.instances[instant_name]
nodes = part.nodes
elements = part.elements
Nele = len(elements)
print("Number of elements: ", Nele)
if Nele == 0:
    raise ValueError("Can't read the model!")
coords = np.array([node.coordinates for node in nodes])
print("Number of nodes: ",np.shape(coords)[0])
bounds0, bounds1 = np.min(coords, axis=0), np.max(coords, axis=0)
# -----------------------------------------------------------------------------
print("---->  Calculating number of seeds to generate...")
MassProp = assembly.getMassProperties(regions=[part, ])
model_volume = MassProp["volume"]
# bound_vol = np.prod(bounds1-bounds0)
# Note: if 1st method is used in generate_seeds function then bound_vol should be used instead of model_volume.
Ngrains = max(1, int(model_volume/((grainsize*1.e-3))**3))
print("Number of grains to start with:", Ngrains)
# -----------------------------------------------------------------------------
print("---->  Generating seeds...")
seeds = generate_seeds(coords, Ngrains)
# -----------------------------------------------------------------------------
print("---->  Erasing existing grains...")
for setname in assembly.sets.keys():
    if setname[:5] == "Grain":
        assembly.deleteSets([setname])
    if setname[:5] == "GRAIN":
        assembly.deleteSets([setname])
# -----------------------------------------------------------------------------
print("---->  Finding element centers...")
ele_centers = np.zeros((Nele, 3))
for i, ele in enumerate(elements):
    ele_centers[i, :] = np.average(coords[ele.connectivity, :], axis=0)
# -----------------------------------------------------------------------------
print("---->  Path and amplitude information...")
pathcoords, pathamps = [], []
for setname in assembly.sets.keys():
    if setname[:4] == "Path":
        pathi = np.array([node.coordinates
                                    for node in assembly.sets[setname].nodes])
        pathcoords.append(pathi)
        AmpDefined = False
        if setname in myModel.amplitudes.keys():
            ampi = np.array(myModel.amplitudes[setname].data)[:, 1]
            if (len(pathi) == len(ampi)):
                AmpDefined = True
        if not AmpDefined:
            print("Error in reading amplitude:", setname)
            print("Either Amplitude is not defined for this path, or has inconsistent number of entries")
            print("defaul amplitude will be used")
            ampi = np.ones(len(pathi)) * default_amp
        pathamps.append(ampi)
#pathcoords, pathamps = [], []
if pathcoords == []:
    Default_Axis = "Z"
    if Default_Axis=="X":
        print("No Path is defined. Using the path: [1, 0, 0]")
        pathcoords.append(np.array([[bounds0[0], 0., 0.], [bounds1[0], 0., 0.]]))
    if Default_Axis=="Y":
        print("No Path is defined. Using the path: [0, 1, 0]")
        pathcoords.append(np.array([[0., bounds0[1], 0.], [0., bounds1[1], 0.]]))
    if Default_Axis=="Z":
        print("No Path is defined. Using the path: [0, 0, 1]")
        pathcoords.append(np.array([[0., 0., bounds0[2]], [0., 0., bounds1[2]]]))
    pathamps.append(np.array([default_amp, default_amp]))
# -----------------------------------------------------------------------------
print('---->  Starting polygonization...')
ids = poly(ele_centers, seeds, pathcoords, pathamps)
print('---->  Generaring sets...')
sets = [[] for i in range(Ngrains)]
for e in range(Nele):
    sets[ids[e]].append(e+1)
j = 0
for i in range(Ngrains):
    if len(sets[i]) > 0:
        j += 1
        assembly.SetFromElementLabels(elementLabels=((instant_name, tuple(sets[i])),),
                                      name='Grain-'+str(j))
# -----------------------------------------------------------------------------
print('---->  Generating texture to assign...')
Nsets = j
# generate random texture
ang_list = np.zeros((Nsets, 3))
for i in range(Nsets):
    tmp = np.random.random(3)
    ang_list[i,:] = 360.*tmp[0], np.arccos(2.*tmp[1]-1.)/np.pi*180., 360.*tmp[2]
# -----------------------------------------------------------------------------
print('---->  Writing texture info in initSDV file...')
content = ['*Initial conditions, type=SOLUTION\n',
           '** elset, phi1, PHI, phi2\n']
for i in range(Nsets):
    content.append('Grain-%d , %.3f, %.3f, %.3f, 0, 0, 0, 0\n'
                   % (i+1, ang_list[i,0],ang_list[i,1],ang_list[i,2]))
    for j in range((Nsdv-7)//8):
        content.append(' 0, 0, 0, 0, 0, 0, 0, 0\n')
    rest0 = (Nsdv-7) % 8
    if rest0 > 0:
        tmp = ' 0,'*rest0
        content.append(tmp[:-1] + '\n')

with open(r'initSDV.inp', 'w') as fout:
    for line in content:
        fout.write(line)
print("======================== F I N I S H E D===============================")
